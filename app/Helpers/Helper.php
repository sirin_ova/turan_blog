<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Helper{

    public static function languages()
    {
        return DB::table('languages')->get();
    }

    public static function lang_count()
    {
        return DB::table('languages')->get()->count();
    }


}
