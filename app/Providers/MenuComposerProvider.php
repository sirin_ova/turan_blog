<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


    /**
     * Register services.
     *
     * @return void
     */

    public function register()
    {
        $this->composeMenu();
    }

    public function composeMenu(){

        view()->composer('layouts.master','App\Http\Composers\MenuComposer');
    }
}
