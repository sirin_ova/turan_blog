<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ContactController extends Controller
{
    public function index()
    {
        $languages = Helper::languages();

        $contacts = DB::table('contacts as C')
            ->join('contact_translates as CT', 'CT.contact_id', '=', 'C.id')
            ->where(['CT.lang_id' => $this->currentLanguage])
            ->select('C.id', 'C.phone', 'CT.country')
            ->orderBy('C.id', 'asc')->get();

        return view('admin.contacts', compact('languages', 'contacts'));
    }

    public function store(Request $request) {

            $id = DB::table('contacts')->insertGetId(['phone' => $request->phone]);

            foreach ($request->country as $key => $country_name) {

                DB::table('contact_translates')->insert([
                    'contact_id' => $id,
                    'lang_id' => $request->lang_id[$key],
                    'country' => $country_name
                ]);
            }
            return $id;

    }

    public function edit($id)
    {
        $data = DB::table('contacts as C')
            ->join('contact_translates as CT', 'C.id', '=', 'CT.contact_id')
            ->select('C.id', 'C.phone', 'CT.country')->where('C.id', $id)->get();
        return $data;
    }

    public function update(Request $request, $id)
    {
        if ($request->phone) {

            DB::table('contacts')->where('id', $id)->update(['phone' => $request->phone]);

            foreach ($request->country as $key => $country_name) {
                DB::table('contact_translates')
                    ->where([
                        ['contact_id', $id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'country' => $country_name
                    ]);
            }
            return 1;
        } else {
            foreach ($request->country as $key => $country_name) {
                DB::table('contact_translates')
                    ->where([
                        ['contact_id', $id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'country' => $country_name
                    ]);
            }
            return 1;
        }

    }

    public function destroy($id)
    {
        DB::table('contacts')->where('id', $id)->delete();
        return response()->json();
    }



}
