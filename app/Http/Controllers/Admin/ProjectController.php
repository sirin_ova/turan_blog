<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProjectController extends Controller
{
    public function index()
    {

        $languages = Helper::languages();

        $projects = DB::table('projects as P')
        ->join('project_translates as PT', 'PT.project_id', '=', 'P.id')
        ->where(['PT.lang_id' => $this->currentLanguage])
        ->select('P.id', 'P.image', 'PT.title', 'PT.description')
        ->orderBy('P.id', 'asc')->get();

        return view('admin.projects', compact('languages', 'projects'));
    }

    public function store(Request $request) {

            if ($request->image) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/blog/') . $filename);

                $id = DB::table('projects')->insertGetId(['image' => $filename]);

                foreach ($request->title as $key => $title_name) {

                    DB::table('project_translates')->insert([
                        'project_id' => $id,
                        'lang_id' => $request->lang_id[$key],
                        'title' => $title_name,
                        'description' => $request->description[$key]
                    ]);
                }
                return $id;
            }
    }

    public function edit($id)
    {
        $data = DB::table('projects as P')
            ->leftJoin('project_translates as PT', 'P.id', '=', 'PT.project_id')
            ->select('P.id', 'P.image', 'PT.title', 'PT.description')->where('P.id', $id)->get();
        return $data;
    }

    public function update(Request $request, $id)
    {

        if ($request->image) {

            $currentImage = Project::find($id)->image;

            if ($request->image != $currentImage) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/blog/') . $filename);

                $currentPhoto = public_path('user/img/blog/') . $currentImage;

                if (file_exists($currentPhoto)) {
                    @unlink($currentPhoto);
                }

                DB::table('projects')->where('id', $id)->update(['image' => $filename]);
            }
                foreach ($request->title as $key => $title_name) {
                    DB::table('project_translates')
                        ->where([
                            ['project_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'description' => $request->description[$key],
                        ]);
                }
                return 1;
            }else{
                foreach ($request->title as $key => $title_name) {
                    DB::table('project_translates')
                        ->where([
                            ['project_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'description' => $request->description[$key],
                        ]);
                }
                return 1;
            }

    }

    public function destroy($id)
    {

        $currentImage = Project::find($id)->image;

        $currentPhoto = public_path('user/img/blog/') . $currentImage;

        if (file_exists($currentPhoto)) {
            unlink($currentPhoto);
        }

        DB::table('projects')->where('id', $id)->delete();
        return response()->json();
    }

    public function open($id)
    {
        $languages = Helper::languages();
        $lang=$this->currentLanguage;

        $project=DB::table('projects as P')
            ->leftJoin('project_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.project_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })
            ->where('P.id', $id)
            ->select('P.id','PT.title')
            ->first();

        if($project){
            $data = DB::table('projects as P')
                ->join('project_translates as PT', 'P.id', '=', 'PT.project_id')
                ->select('PT.text')->where('P.id', $id)->get();

            $description=[];
            foreach($data as $key=>$value){
                $description[$key]=$value->text;
            }
            $description = collect($description);

            return view('admin.project_open',compact('description','languages','project'));
        }

    }

    public function save(Request $request, $id)
    {
        $project=DB::table('projects')->where('id',$id)->select('id')->first();

        if($project){
            foreach ($request->description as $key => $description_name) {
                DB::table('project_translates')
                    ->where([
                        ['project_id', $project->id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'text' => $description_name
                    ]);
            }
            return 1;
        }
    }

}
