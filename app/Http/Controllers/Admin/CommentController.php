<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Professional;
use File;


class CommentController extends Controller
{
    public function index(){

        $languages = Helper::languages();

        $edit_route = route('admin.comments');
        $save_route = route('admin.comments.store');

        $comments = DB::table('comments as C')
            ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
            ->where(['CT.lang_id' => $this->currentLanguage])
            ->select('C.id', 'CT.title', 'CT.text')
            ->orderBy('C.id', 'asc')->get();

        return view('admin.comments', compact('languages', 'comments','edit_route', 'save_route'));

    }

    public function store(Request $request)
    {
        $id = DB::table('comments')->insertGetId(['isVisible' => $request->visible]);
        foreach ($request->title as $key => $title_name) {
            DB::table('comment_translates')->insert([
                'comment_id' => $id,
                'lang_id' => $request->lang_id[$key],
                'title' => $title_name,
                'text' => $request->description[$key]
            ]);
        }
        return $id;

    }

    public function edit($id){

        $data = DB::table('comments as C')
            ->join('comment_translates as CT', 'C.id', '=', 'CT.comment_id')
            ->select('C.id', 'CT.title', 'CT.text')->where('C.id', $id)->get();
        return $data;

    }

    public function update(Request $request, $id)
    {
        foreach ($request->title as $key => $title_name) {
            DB::table('comment_translates')
                ->where([
                    ['comment_id', $id],
                    ['lang_id', $request->lang_id[$key]]
                ])
                ->update([
                    'title' => $title_name,
                    'text' => $request->description[$key],
                ]);
        }
        return 1;

    }


    public function destroy($id)
    {
        DB::table('comments')->where('id', $id)->delete();
        return response()->json();
    }


}
