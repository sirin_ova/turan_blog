<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BiographyController extends Controller
{
    public function index()
    {
        $languages = Helper::languages();

        $bio=DB::table('biography_translates')
            ->select('biography_translates.id','biography_translates.text')
            ->get();
        $bio_id=1;
            $description=[];
            foreach($bio as $key=>$value){
                $description[$key]=$value->text;
            }
            $description = collect($description);

            return view('admin.biography',compact('description','bio_id','languages'));
//        }

    }

    public function save(Request $request, $id)
    {
           foreach ($request->description as $key => $description_name) {
                DB::table('biography_translates')
                    ->where([
                        ['bio_id',$id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'text' => $description_name
                    ]);
            }
            return 1;

    }
}
