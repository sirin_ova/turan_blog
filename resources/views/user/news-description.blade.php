@extends('layouts.master')
@section('contain')
<main>
    <!-- |=====|| Blog Single Start ||===============| -->
    <section class="blog_single1">
        <div class="content_box_120_70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="blog_single1__wrapper">
                            {!! $news->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- |=====||  End ||=================| -->
</main>
@endsection
