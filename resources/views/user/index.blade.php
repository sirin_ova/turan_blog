@extends('layouts.master')
@section('contain')
    <main>
        <!-- |=====|| Banner Start ||===============| -->
        <section class="banner">
            <div class="swiper-container s1">
                <div class="swiper-wrapper">
                    @foreach($sliders as $slider)
                        <div class="swiper-slide">
                            <div class="hero1__bg--01" style="background-image: url('user/img/bg/{{$slider->image}}');">
                                <div class="image-box_overlay"></div>
                                <div class="hero1__wrapper hero1__height d-flex align-items-center justify-content-center">-->
                                    <div class="hero1__content-box text-center">
                                        <h3>{{$slider->subtitle}}</h3>
                                        <h1>{{$slider->title}}</h1>
                                        <h4 class="copyright">{{$slider->author}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <!-- |=====|| Banner End ||=================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Working-process Start ||===============| -->
        <section class="working-process">
            <div class="content_box_120_90">
                <div class="container">
                    <div class="row">
                        @foreach($professionals as $professional)
                            <div class="col-lg-4 col-xl-3 col-sm-6">
                                <div class="working-process__box text-center mb-30">
                                    <div class="working-process__thumb">
                                        <img src="{{asset('user/img/png-icon')}}/{{$professional->icon}}" alt="working-process icon">
                                    </div>
                                    <h4>{{$professional->title}}</h4>
                                    <p>{{$professional->description}}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Working-process End ||=================| -->
        <!-- |==========================================| -->


        <!-- |==========================================| -->
        <!-- |=====|| About Start ||===============| -->
        <section class="about1">
            <div class="content_box_pob_120">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="about1__img">
                                <img src="{{asset('user/img/blog')}}/{{$abouts->image}}" alt="About img">
                            </div>
                        </div>
                        <div class="col-lg-6 d-flex align-items-center">
                            <div class="about1__content">
                                <div class="sub__title">
                                    <h3>BİZ KİMİK?</h3>
                                </div>
                                <div class="title mb-18">
                                    <h2>{{$abouts->title}}</h2>
                                </div>
                                <p class="m-0">{{$abouts->description}}</p>
                                <div class="about1__item mt-30">
                                    <div class="about1__thumb">
                                        <img src="{{asset('user/img/png-icon/about-02.png')}}" alt="">
                                    </div>
                                    <div class="about1__text">
                                        <h3>32 illik təcrübə</h3>
                                        <p>Daha yaxşı nəticələriniz üçün 32 illik təcrübəmizlə Sizinləyik!</p>
                                    </div>
                                </div>
                                <div class="about1__item mt-30">
                                    <div class="about1__thumb">
                                        <img src="{{asset('user/img/png-icon/about-01.png')}}" alt="">
                                    </div>
                                    <div class="about1__text">
                                        <h3>Uğurlu layihələrə atılan imza</h3>
                                        <p class="m-0">Uğura imza atmış Port Baku Bazar layihəsi Azərbaycanın ilk və tək VİP mağazasıdır ki, açılışı Turan Özbahceci tərəfindən reallaşdırılmışdır. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| About End ||=================| -->
        <!-- |==========================================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Service Start ||===============| -->
        <section class="service1">
            <div class="content_box_pob_120_70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title mb-65 text-center">
                                <h2>Layihələrimiz</h2>
                            </div>
                        </div>
                    </div>
                        <div class="swiper-container s2">
                            <div class="swiper-wrapper">
                                @foreach($projects as $project)
                                    <div class="swiper-slide">
                                        <div class="service1__item mb-50">
                                                <div class="service1__thumb">
                                                    <img src="{{asset('user/img/blog')}}/{{$project->image}}" alt="Service Img">
                                                </div>
                                                <div class="service1__content">
                                                    <h3><a href="project-description.blade.php">{{$project->title}}</a></h3>
                                                    <p class="m-0">{{$project->description}}</p>
                                                </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination swiper-pagination2"></div>
                        </div>
                    <button class="see-more-btn">
                        <a class="see-more" href="{{route('projects')}}">Daha ətraflı</a>
                    </button>


                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Service End ||================| -->
        <!-- |==========================================| -->





        <!-- |==========================================| -->
        <!-- |=====|| Fun facts Start ||===============| -->
        <section class="fun-facts">
            <div class="content_box_120_70">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <div class="fun-facts__item mb-50 text-center">
                                <h3 class="counter">4</h3>
                                <p class="m-0">Tamamlanmış Layihələr</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="fun-facts__item mb-50 text-center">
                                <h3 class="counter">8</h3>
                                <p class="m-0">Davam Edən Layihələr</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="fun-facts__item mb-50 text-center">
                                <h3 class="counter">15</h3>
                                <p class="m-0">Yeni Layihələr</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="fun-facts__item mb-50 text-center">
                                <h3 class="counter">112</h3>
                                <p class="m-0">Məmnun müştəri</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Fun facts End ||=================| -->
        <!-- |==========================================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Service Start ||===============| -->
        <section class="service1">
            <div class="content_box_pob_120_70 mt-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title mb-65 text-center">
                                <h2>Xəbərlər</h2>
                            </div>
                        </div>
                    </div>


                    <div class="swiper-container s3">
                        <div class="swiper-wrapper">
                            @foreach($news as $data)
                                <div class="swiper-slide">
                                    <div class="service1__item mb-50">
                                        <div class="service1__thumb">
                                            <img src="{{asset('user/img/blog/')}}/{{$data->image}}" alt="Service Img">
                                        </div>
                                        <div class="service1__content">
                                            <h3><a href="news-description.blade.php">{{$data->title}}</a></h3>
                                            <p class="m-0">{{$data->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination swiper-pagination3"></div>

                    </div>
                    <button class="see-more-btn">
                        <a class="see-more" href="{{route('news')}}">Daha ətraflı</a>
                    </button>
                </div>
            </div>
        </section>
        <!-- |=====|| Service End ||=================| -->
        <!-- |==========================================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Client Start ||===============| -->
        <section class="client1">
            <div class="pb-120 pt-75">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="client1__title mb-70">
                                <div class="title text-center">
                                    <h2>Tərəfdaşlarımız</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="client1__active owl-carousel">
                                @foreach($partners as $partner)
                                    <div class="client1__item d-flex justify-content-center align-items-center">
                                        <div class="client1__thumb">
                                            <a href="#"><img src="{{asset('user/img/client/')}}/{{$partner->image}}" alt="Client"></a>
                                        </div>
                                    </div>
                                @endforeach
{{--                                <div class="client1__item d-flex justify-content-center align-items-center">--}}
{{--                                    <div class="client1__thumb">--}}
{{--                                        <a href="#"><img src="{{asset('user/img/client/a-3.png')}}" alt="Client"></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Client End ||=================| -->
        <!-- |==========================================| -->
    </main>
@endsection
