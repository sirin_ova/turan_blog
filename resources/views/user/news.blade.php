@extends('layouts.master')
@section('css')
    <style>
        .page-item{
            display:inline-block;
        }
        .pagination-nav nav{
            margin:0 auto;
        }
        .page-item.active .page-link{
            background:black !important;
            border-color: black !important;
            color:white !important;
        }
        .page-link{
            color:black !important;
        }

    </style>

@section('contain')
    <main>
        <!-- |==========================================| -->
        <!-- |=====|| Page Title Start ||===============| -->
        <section class="page_title page_title__bg-14">
            <div class="page_title__padding">
                <div class="page_title__content text-center">
                    <h1>Xəbərlər</h1>
                </div>
            </div>
        </section>
        <!-- |=====|| Page Title End ||=================| -->
        <!-- |==========================================| -->

        <!-- |==========================================| -->
        <!-- |=====|| Blog Start ||===============| -->
        <section class="blog_page1">
            <div class="content_box_120_70">
                <div class="container">
                    <div class="row">
                        @foreach($news as $data)
                            <div class="col-md-6">
                                <div class="blog_page1__item mb-50">
                                    <div class="blog_page1__thumb">
                                        <img src="{{asset('user/img/blog')}}/{{$data->image}}" alt="Blog Image" class="img_390">
                                    </div>
                                    <div class="blog_page1__data mt-35">
                                        <a href="#"><i class="ti-calendar"></i>{{date('d M, m',strtotime($data->created_at))}}</a>
                                    </div>
                                    <div class="blog_page1__content">
                                        <h3>{{$data->title}}</h3>
                                        <p>{{$data->description}}</p>
                                        <a href="{{route('news-description',$data->id)}}" class="site_btn_09">Davam et<i class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                   <div class="row pagination-nav">
                       {{$news->links()}}
                   </div>

                </div>
            </div>
        </section>
        <!-- |=====|| Blog End ||=================| -->
        <!-- |==========================================| -->
    </main>
@endsection
