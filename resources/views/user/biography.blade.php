@extends('layouts.master')
@section('contain')
    <main>
        <!-- |=====|| Banner Start ||===============| -->
        <section class="banner">
            <div class="swiper-container s1">
                <div class="swiper-wrapper">
                    @foreach($sliders as $slider)
                    <div class="swiper-slide">
                        <div class="hero1__bg--01 pt-150" style="background-image: url('{{url('/')}}/user/img/bg/{{$slider->image}}')">
                            <div class="image-box_overlay pt-150"></div>
                            <div class="hero1__wrapper hero1__height d-flex align-items-center justify-content-center">
                                <div class="hero1__content-box text-center">
                                    <h1 class="hero-word">{{$slider->title}}</h1>
                                    <h4 class="copyright">{{$slider->author}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <!-- |=====|| Banner End ||=================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Blog Single Start ||===============| -->
        <section class="blog_single1">
            <div class="content_box_120_70">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="blog_single1__wrapper">
                             {!! $bio->text !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====||  End ||=================| -->
        <!-- |==========================================| -->
    </main>
@endsection
