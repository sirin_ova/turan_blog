<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('slider_id')->unsigned();
            $table->integer('lang_id');
            $table->string('title');
            $table->string('subtitle');
            $table->string('author');
            $table->timestamps();
        });

        Schema::table('slider_translates', function (Blueprint $table) {
            $table->foreign('slider_id')->references('id')->on('sliders')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_translates');
    }
}
